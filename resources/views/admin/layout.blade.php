<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Shopify_zandz</title>
        <!-- FONT -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        
        <!-- SLIDE SLICK -->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css"/> 
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.css"/>

        <!-- BOOTSTRAP -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <!-- CSS -->
        <link rel="stylesheet" href="{{ asset('/admin/css/main.css') }}">
        

        
    </head>
<body>
    <!-- HEADER -->
    <header>
        <div class="header-menu--logo">
          <img id="logo_header" src="{{ asset('/images/logo.png') }}">
        </div>
        <div class="menubar">
            <div class="container">
                <div class="icon-menu">
                    <i class="fa fa-bars"></i>
                </div>
                <div class="bars">
                    <ul>
                        <li><a href="/wp-admin/home">Home</a></li>
                        <li><a href="/wp-admin/about">About</a></li>
                        <li><a href="/wp-admin/product">Product</a></li>
                        <li><a href="/wp-admin/news">News</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- END HEADER -->
    <div class="container">
    @yield('content')   
    </div>

    <!-- FOOTER -->
    <div class="footer">
        <div class="footer-bottom" style="text-align: center;">
          <p>copyright © 2018 z and z skateboards. all rights reserved.</p>
        </div>
      </div>
    </div>
    <!-- END FOOTER -->




    
    
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
        <script type="text/javascript" src="{{ asset('/admin/js/main.js') }}"></script>
   
</body>
</html>
