<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Shopify_zandz</title>
        <!-- FONT -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- CSS -->
        <link rel="stylesheet" href="{{ asset('/css/main.css') }}">
        <!-- SLIDE SLICK -->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css"/> 
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.css"/>

        <!-- BOOTSTRAP -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
        <link rel="stylesheet" href="{{ asset('/js/main.js') }}">
  
    </head>
<body>
    <!-- HEADER -->
    <header>
        <div class="container-flust">
            <div class="row header_contract">
                <div class="header_contract-left col-md-4 col-xs-12">
                    <ul class="header_contract-left-list">
                        <li class="header_contract-left--phone">
                            <a href="#"><i class="fa fa-phone" aria-hidden="true"></i></a>
                            (213) 915-8539
                        </li>
                        <li class="header_contract-left--email">
                            <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                            sales@zzskateboards.com
                        </li>
                    </ul>
                </div>
                <div class="header-menu--logo col-md-4 col-xs-12">
                  <img id="logo_header" src="{{ asset('/images/logo.png') }}">
                </div>
                <div class="header_contract-right col-md-4 col-xs-12">
                    <ul  class="header_contract-right-list">
                        <li class="header_contract-right--login">
                            <a href="#"><i class="fa fa-user" aria-hidden="true"></i></a>Login
                        </li>
                        <li class="header_contract-right--regis">
                            <a href="#"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>Register
                        </li>
                        <li class="header_contract-right--cart">
                            <a href="#"><i class="fa fa-cart-arrow-down" aria-hidden="true"></i></a>Cart
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="header-menu">
            <div class="">
                <!-- <a class="navbar-brand" href="#"></a> -->
                
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse header-menu--list" id="navbarNavDropdown">
                  <ul class="navbar-nav navbar-nav-left">
                    <li class="nav-item active">
                      <a class="nav-link menu_home" href="/">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="/about">About</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="/products">Products</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="/news">News</a>
                    </li>
                  </ul>
                  
                </div>
            </div>
        </div>
        
    </header>
    <!-- END HEADER -->

    <div class="container">
    @yield('content')   
    </div>


    <!-- FOOTER -->
    <div class="footer">       
        <div class="footer_end-bottom">
          copyright © 2018 z and z skateboards. all rights reserved.
        </div>     
    </div>
      <!-- END FOOTER -->




    
    

   
</body>
</html>
