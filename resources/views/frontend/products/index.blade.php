@extends('frontend.index')
 
@section('content')
    
   <div class="list-product row">

        @foreach ($products as $product)
        <div class="product-item col-md-4">
            <h5 class="category">{{ $product->category }}</h5>
            <h4 class="title"><a href="{{ route('products.show',$product->id) }}">{{ $product->name }}</a></h4>
            <p class="price">{{ $product->price }}</p>
            <div class="description">
                {{ str_limit($product->detail, 100) }}
            </div>
        </div>
        @endforeach
    
   </div>
    
   
    
  
    {!! $products->links() !!}
      
@endsection