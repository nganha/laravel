jQuery(document).ready(function(){
	jQuery('.icon-menu').on('click', function() {
		if(jQuery('.bars').hasClass('open')){
			jQuery('.bars').removeClass('open');
		}else{
			jQuery('.bars').addClass('open');
		}

	});
	const $menu = jQuery('.bars');
	// close menu
	jQuery(document).mouseup(function(e) {
		if (!$menu.is(e.target) && $menu.has(e.target).length === 0){
		    $menu.removeClass('open');
		}
	});	
});