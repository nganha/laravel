<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class LayoutProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::latest()->paginate(6);
  
        return view('frontend.products.index',compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 6);
    }
    public function show($product_id)
    {
        $product = Product::where('id',$product_id)->first();
        return view('frontend.products.show',compact('product'));
    }
   
   
   
}
