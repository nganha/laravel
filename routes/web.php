<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index');
Route::get('/products','LayoutProductController@index')->name('products.index');
Route::get('/wp-admin','AdminController@index');
Route::get('/wp-admin/about','AboutController@index');
Route::get('/wp-admin/products','ProductController@index')->name('products.index');
Route::get('/wp-admin/products/create','ProductController@create')->name('products.create');
Route::get('/wp-admin/products/edit/{product_id}','ProductController@edit')->name('products.edit');
Route::post('/wp-admin/products/store','ProductController@store')->name('products.store');
Route::put('/wp-admin/products/update/{product_id}','ProductController@update')->name('products.update');
Route::delete('/wp-admin/products/destroy/{product_id}','ProductController@destroy')->name('products.destroy');
Route::get('/wp-admin/products/show/{product_id}','ProductController@show')->name('products.show');
Route::get('/wp-admin/news','NewController@index');